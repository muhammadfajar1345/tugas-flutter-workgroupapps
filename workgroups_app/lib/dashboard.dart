import 'package:flutter/material.dart';
import 'package:workgroups_app/menucontrol.dart';
import 'package:workgroups_app/login.dart';
import 'package:workgroups_app/classmate.dart';

void main() => runApp(Test());

class Test extends StatelessWidget {
  String txtUsername;
  Test({this.txtUsername});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Dashboard(),
    );
  }
}

class Dashboard extends StatefulWidget {
  String txtUsername;
  Dashboard({Key key, this.txtUsername}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState(txtUsername);
}

class _DashboardState extends State<Dashboard> {
  int _selectedIndex = 0;
  String txtUsername;
  _DashboardState(this.txtUsername);
  static TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Widget> _widgetOptions = <Widget>[
    Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            "WELCOMEBACK MATES",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Color(0xFF00B0FF),
              fontWeight: FontWeight.bold,
              fontSize: 30.0,
            ),
          ),
          Text(
            "MUHAMMAD FAJAR WIBISONO",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Color(0xFF00B0FF),
              fontWeight: FontWeight.bold,
              fontSize: 20.0,
            ),
          ),
          SizedBox(
            height: 55.0,
          ),
          Image.asset(
            'assets/profile.jpg',
            height: 150.0,
            width: 150.0,
          ),
          SizedBox(
            height: 55.0,
          ),
          FlatButton(
            minWidth: 100,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
                side: BorderSide(color: Color(0xFF00B0FF))),
            child: Text(
              'Check Profile',
              style: TextStyle(fontSize: 20.0),
            ),
            color: Colors.white,
            textColor: Color(0xFF00B0FF),
            onPressed: () {},
          )
        ],
      ),
    ),
    Classmates(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions[_selectedIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.people),
            label: 'Clasemate',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.logout),
            label: 'Logout',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Color(0xFF00B0FF),
        onTap: _onItemTapped,
      ),
    );
  }
}
