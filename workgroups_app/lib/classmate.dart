import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Classmates extends StatefulWidget {
  @override
  _ClassmatesState createState() => _ClassmatesState();
}

class _ClassmatesState extends State<Classmates> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF00B0FF),
      body: Center(
          child: GridView.extent(
        primary: false,
        padding: const EdgeInsets.fromLTRB(16, 45, 16, 16),
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        maxCrossAxisExtent: 200.0,
        children: <Widget>[
          Card(
            elevation: 10,
            child: Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 10.0,
                  ),
                  Image.asset(
                    'assets/profile.jpg',
                    height: 100.0,
                    width: 100.0,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    "Muhammad Fajar WIbisono",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xFF00B0FF),
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            elevation: 10,
            child: Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 10.0,
                  ),
                  SvgPicture.asset(
                    'assets/female.svg',
                    height: 100.0,
                    width: 100.0,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    "Person 2",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xFF00B0FF),
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            elevation: 10,
            child: Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 10.0,
                  ),
                  SvgPicture.asset(
                    'assets/male.svg',
                    height: 100.0,
                    width: 100.0,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    "Person 3",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xFF00B0FF),
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            elevation: 10,
            child: Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 10.0,
                  ),
                  SvgPicture.asset(
                    'assets/female.svg',
                    height: 100.0,
                    width: 100.0,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    "Person 4",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xFF00B0FF),
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            elevation: 10,
            child: Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 10.0,
                  ),
                  SvgPicture.asset(
                    'assets/male.svg',
                    height: 100.0,
                    width: 100.0,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    "Person 5",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xFF00B0FF),
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            elevation: 10,
            child: Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 10.0,
                  ),
                  SvgPicture.asset(
                    'assets/male.svg',
                    height: 100.0,
                    width: 100.0,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    "Person 6",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xFF00B0FF),
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            elevation: 10,
            child: Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 10.0,
                  ),
                  SvgPicture.asset(
                    'assets/male.svg',
                    height: 100.0,
                    width: 100.0,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    "Person 7",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xFF00B0FF),
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            elevation: 10,
            child: Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 10.0,
                  ),
                  SvgPicture.asset(
                    'assets/female.svg',
                    height: 100.0,
                    width: 100.0,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    "Person 8",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xFF00B0FF),
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            elevation: 10,
            child: Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 10.0,
                  ),
                  SvgPicture.asset(
                    'assets/female.svg',
                    height: 100.0,
                    width: 100.0,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    "Person 9",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xFF00B0FF),
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            elevation: 10,
            child: Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 10.0,
                  ),
                  SvgPicture.asset(
                    'assets/male.svg',
                    height: 100.0,
                    width: 100.0,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    "Person 10",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xFF00B0FF),
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      )),
    );
  }
}
